package com.nikitavasin.testweatherappsoftbalance.retrofit

import com.nikitavasin.testweatherappsoftbalance.retrofit.data.WeatherResponce
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query


interface Api {

    @GET("onecall")
    fun getWeather(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") appid: String,
        @Query("units") units: String
    ) : Flowable<WeatherResponce>

}