package com.nikitavasin.testweatherappsoftbalance.retrofit.data

data class City(
    val coord: Coord,
    val country: String,
    val id: Int,
    val name: String,
    val state: String
)