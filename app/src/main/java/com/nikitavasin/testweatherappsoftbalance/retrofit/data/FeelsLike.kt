package com.nikitavasin.testweatherappsoftbalance.retrofit.data

data class FeelsLike(
    val day: Double,
    val eve: Double,
    val morn: Double,
    val night: Double
)