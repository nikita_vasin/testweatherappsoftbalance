package com.nikitavasin.testweatherappsoftbalance.retrofit.data

data class WeatherResponce(
    val daily: List<Daily>,
    val lat: Double,
    val lon: Double,
    val timezone: String,
    val timezone_offset: Int
)