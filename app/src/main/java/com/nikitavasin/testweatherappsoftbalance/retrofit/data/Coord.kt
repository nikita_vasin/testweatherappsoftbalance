package com.nikitavasin.testweatherappsoftbalance.retrofit.data

data class Coord(
    val lat: Double,
    val lon: Double
)