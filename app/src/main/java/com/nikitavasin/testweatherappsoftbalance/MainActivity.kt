package com.nikitavasin.testweatherappsoftbalance

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.nikitavasin.testweatherappsoftbalance.di.ActivityModule
import com.nikitavasin.testweatherappsoftbalance.di.DaggerComponent
import com.nikitavasin.testweatherappsoftbalance.retrofit.Api
import com.nikitavasin.testweatherappsoftbalance.retrofit.data.Coord
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity(),
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
    LocationListener {

    companion object {
        const val PERMISSION_REQUEST_CODE: Int = 0
        const val API_KEY : String = "5d8bc249fcf0cd136203b47f1436c8db"
    }

    private var mLocation: Location? = null
    private var searchDispose: Disposable? = null
    private var mLocationRequest: LocationRequest? = null

    @Inject
    lateinit var locationManager: LocationManager
    @Inject
    lateinit var mGoogleApiClient: GoogleApiClient
    @Inject
    lateinit var adapter: WeatherAdapter
    @Inject
    lateinit var layoutManager: LinearLayoutManager
    @Inject
    lateinit var api: Api
    @Inject
    lateinit var geoCoder : Geocoder


    @SuppressLint("ShowToast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DaggerComponent.builder()
            .activityModule(ActivityModule(this))
            .build()
            .injectMain(this)

        rvWeather.layoutManager = layoutManager
        rvWeather.adapter = adapter

        btnLocate.setOnClickListener{
            detectPosition()
        }

        btnSearch.setOnClickListener{
            search()
        }

        etSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                search()
                true
            } else false
        }
    }

    private fun search() {
        if (etSearch.text.toString().isNotEmpty()) {
            val coord: Coord? = getLocation(etSearch.text.toString())
            if(coord != null) request(coord.lon, coord.lat)
            else Toast.makeText(this, "Not Found", Toast.LENGTH_SHORT).show()
            etSearch.setText("")
        } else Toast.makeText(this, "Search is empty", Toast.LENGTH_SHORT).show()
    }

    @SuppressLint("ShowToast")
    private fun request(lon: Double, lat: Double) {
        searchDispose?.dispose()
        searchDispose = api.getWeather(
            lon = lon,
            lat = lat,
            appid = API_KEY,
            units = "metric"
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({weather ->
                tvLocation.text = getCityName(lat, lon)
                adapter.updateItems(ArrayList(weather.daily))
                searchDispose?.dispose()
            }, {
                Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                searchDispose?.dispose()
            })
    }

    private fun getLocation(city: String) : Coord? {

        val addresses: List<Address>? = geoCoder?.getFromLocationName(city, 5)
        val lat: Double? = addresses?.getOrNull(0)?.latitude
        val lon: Double? = addresses?.getOrNull(0)?.longitude

        return if (lat != null && lon != null) {
            Coord(lat, lon)
        } else null
    }

    private fun getCityName(lat : Double, lon: Double) : String {

        val addresses: List<Address>? = geoCoder?.getFromLocation(lat, lon, 5)
        val cityName: String? = addresses?.getOrNull(0)?.getAddressLine(0)

        return cityName.toString()
    }

    private fun requestPermissions() {
        val permissions = arrayOf(ACCESS_FINE_LOCATION,ACCESS_COARSE_LOCATION)
        ActivityCompat.requestPermissions(this, permissions,PERMISSION_REQUEST_CODE)
    }

    override fun onConnected(bundle: Bundle?) {
        detectPosition()
    }

    private fun detectPosition() {
        if (ActivityCompat.checkSelfPermission(
                this,
                ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions()
            return
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        if (mLocation == null) {
            startLocationUpdates()
        }
        if (mLocation != null) {
            val latitude = mLocation?.latitude
            val longitude = mLocation?.longitude
            request(longitude!!, latitude!!)
        } else {
            Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    private fun startLocationUpdates() {
        mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(3600)
            .setFastestInterval(3600)
        if (ActivityCompat.checkSelfPermission(
                this,
                ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions()
            return
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
            mGoogleApiClient,
            mLocationRequest,
            this
        )
    }

    override fun onConnectionSuspended(i: Int) {
        mGoogleApiClient.connect()
    }

    override fun onStart() {
        super.onStart()
        mGoogleApiClient.connect()
    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient.isConnected) {
            mGoogleApiClient.disconnect()
        }
        searchDispose?.dispose()
    }

    override fun onLocationChanged(location: Location?) {}
    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Toast.makeText(this, "Location connect failed", Toast.LENGTH_SHORT).show()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    detectPosition()
                } else {
                    Toast.makeText(this, "Location is disabled. Use search field", Toast.LENGTH_SHORT).show()
                }
                return
            }
            else -> {
                // Ignore all other requests.
            }
        }
    }

}