package com.nikitavasin.testweatherappsoftbalance.di

import android.content.Context
import com.google.android.gms.common.api.GoogleApiClient
import com.nikitavasin.testweatherappsoftbalance.MainActivity
import dagger.Module
import dagger.Provides

@Module
class ActivityModule (var context: MainActivity) {

    @Provides
    fun provideContext (): Context {
        return context
    }

    @Provides
    fun provideConnectionCallback (): GoogleApiClient.ConnectionCallbacks {
        return context
    }

    @Provides
    fun providesConnectionFailedListener () : GoogleApiClient.OnConnectionFailedListener {
        return context
    }

}