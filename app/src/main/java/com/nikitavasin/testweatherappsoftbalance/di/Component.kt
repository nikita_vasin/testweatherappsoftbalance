package com.nikitavasin.testweatherappsoftbalance.di

import com.nikitavasin.testweatherappsoftbalance.MainActivity
import dagger.Component

@Component(modules = [
    ActivityModule::class,
    AdapterModule::class,
    ApiModule::class,
    LocationModule::class
])
interface Component {

    fun injectMain(activity: MainActivity)

}