package com.nikitavasin.testweatherappsoftbalance.di

import android.content.Context
import android.location.Geocoder
import android.location.LocationManager
import com.nikitavasin.testweatherappsoftbalance.MainActivity
import dagger.Module
import dagger.Provides
import java.util.*

@Module
class LocationModule {

    @Provides
    fun provideGeoCoder(context: Context) : Geocoder {
        return Geocoder(context, Locale.getDefault())
    }

    @Provides
    fun provideLocationManager(context: Context) : LocationManager {
        return context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

}