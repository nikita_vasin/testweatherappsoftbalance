package com.nikitavasin.testweatherappsoftbalance.di

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import com.nikitavasin.testweatherappsoftbalance.MainActivity
import com.nikitavasin.testweatherappsoftbalance.WeatherAdapter
import dagger.Module
import dagger.Provides

@Module(includes = [ActivityModule::class])
class AdapterModule {

    @Provides
    fun provideAdapter() : WeatherAdapter {
        return WeatherAdapter()
    }

    @Provides
    fun provideLayoutManager(context: Context) : LinearLayoutManager {
        return LinearLayoutManager(context)
    }

}