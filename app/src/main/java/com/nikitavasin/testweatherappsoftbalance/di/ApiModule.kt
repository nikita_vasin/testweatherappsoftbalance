package com.nikitavasin.testweatherappsoftbalance.di

import android.content.Context
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.nikitavasin.testweatherappsoftbalance.retrofit.Api
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module(includes = [ActivityModule::class])
class ApiModule {

    @Provides
    fun provideWeatherApi () : Api {
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://api.openweathermap.org/data/2.5/")
            .build()
            .create(Api::class.java)
    }

    @Provides
    fun provideGoogleClient (
        context: Context,
        callback: GoogleApiClient.ConnectionCallbacks,
        onConnectionFailedListener: GoogleApiClient.OnConnectionFailedListener
    ) : GoogleApiClient {
        return GoogleApiClient.Builder(context)
            .addConnectionCallbacks(callback)
            .addOnConnectionFailedListener(onConnectionFailedListener)
            .addApi(LocationServices.API)
            .build()
    }

}