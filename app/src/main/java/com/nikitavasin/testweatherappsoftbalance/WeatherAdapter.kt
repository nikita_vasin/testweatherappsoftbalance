package com.nikitavasin.testweatherappsoftbalance

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nikitavasin.testweatherappsoftbalance.retrofit.data.Daily
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class WeatherAdapter : RecyclerView.Adapter<WeatherAdapter.CityHolder>() {

    private var cityList: ArrayList<Daily> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.weather_rv_item, parent, false)
        return CityHolder(view)
    }

    override fun getItemCount(): Int {
        return cityList.size
    }

    override fun onBindViewHolder(holder: CityHolder, position: Int) {
        holder.bind(cityList[position])
    }

    class CityHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bind(daily: Daily) {
            itemView.findViewById<TextView>(R.id.tvTemp).text =
                "from ${daily.temp.min},C to ${daily.temp.max},C"
            val calendar: Calendar = GregorianCalendar()
            calendar.time = Date(daily.dt.toLong())
            itemView.findViewById<TextView>(R.id.tvDate).text =
                getDate(daily.dt*1000, "dd.MM.yyyy")
        }

        @SuppressLint("SimpleDateFormat")
        fun getDate(milliSeconds: Long, dateFormat: String?): String? {
            val formatter = SimpleDateFormat(dateFormat)
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds
            return formatter.format(calendar.time)
        }
    }

    fun updateItems(list : ArrayList<Daily>) {
        cityList = list
        notifyDataSetChanged()
    }

}


